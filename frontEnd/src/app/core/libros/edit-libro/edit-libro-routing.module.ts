import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditLibroComponent } from './edit-libro.component';

const routes: Routes = [
  {
    path: '',
    component: EditLibroComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditLibroRoutingModule { }
