import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditLibroRoutingModule } from './edit-libro-routing.module';
import { EditLibroComponent } from './edit-libro.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    EditLibroComponent
  ],
  imports: [
    CommonModule,
    EditLibroRoutingModule,
    FormsModule
  ]
})
export class EditLibroModule { }
