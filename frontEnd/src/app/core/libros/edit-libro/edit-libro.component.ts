import { Component } from '@angular/core';
import { LibrosService } from '../service/libros.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-libro',
  templateUrl: './edit-libro.component.html',
  styleUrls: ['./edit-libro.component.scss']
})
export class EditLibroComponent {
  titulo:any = '';
  autor:any = '';
  fecha:any = '';
  categoria:any = '';

  valid_form:boolean = false;

  book_id:any;

  valid_form_success:boolean = false;

  text_validation:any = null;

  constructor(
    public LibrosService: LibrosService,
    public activedRoute: ActivatedRoute,
  ){

  }

  ngOnInit(): void
  {
    this.activedRoute.params.subscribe((resp:any) => {
      console.log(resp);
      this.book_id = resp.id;
    });

    this.showBook();
  }

  showBook()
  {
    this.LibrosService.showBook(this.book_id).subscribe((resp:any) => {
      console.log(resp);
      this.titulo = resp.titulo;
      this.autor = resp.autor;
      this.fecha = resp.fecha;
      this.categoria = resp.categoria;
    });
  }

  save()
  {
    if (!this.titulo || !this.autor || !this.fecha || !this.categoria)
    {
      this.valid_form = true;
    }
    else
    {
      let data = {
        titulo: this.titulo,
        autor: this.autor,
        fecha: this.fecha,
        categoria: this.categoria
      };

      this.valid_form = false;
      this.valid_form_success = false;
      this.LibrosService.editBooks(data, this.book_id).subscribe((resp:any) => {
        console.log(resp);
        if(resp.message == 403)
        {
          this.text_validation = resp.message_text;
        }
        else
        {
          this.valid_form_success = true;
        }
      
      });
    }

    
  }

}
