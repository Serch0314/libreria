import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LibrosComponent } from './libros.component';

const routes: Routes = [
  { 
    path: '',
    component: LibrosComponent,
    children: [
      {
        path: 'add-libro',
        loadChildren: () =>
          import('./add-libro/add-libro.module').then((m) => m.AddLibroModule),
      },
      {
        path: 'list-libro',
        loadChildren: () =>
          import('./list-libro/list-libro.module').then((m) => m.ListLibroModule),
      },

      {
        path: 'list-libro/edit/:id',
        loadChildren: () =>
          import('./edit-libro/edit-libro.module').then((m) => m.EditLibroModule),
      },
      
    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibrosRoutingModule { }
