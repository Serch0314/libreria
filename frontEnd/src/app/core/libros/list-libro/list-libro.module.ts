import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListLibroRoutingModule } from './list-libro-routing.module';
import { ListLibroComponent } from './list-libro.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ListLibroComponent
  ],
  imports: [
    CommonModule,
    ListLibroRoutingModule,
    FormsModule,
  ]
})
export class ListLibroModule { }
