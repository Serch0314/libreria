import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LibrosService } from '../service/libros.service';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-list-libro',
  templateUrl: './list-libro.component.html',
  styleUrls: ['./list-libro.component.scss']
})
export class ListLibroComponent {
  public booksList:any = [];
  dataSource!: MatTableDataSource<any>;

  public showFilter = false;
  public searchDataValue = '';
  public lastIndex = 0;
  public pageSize = 10;
  public totalData = 0;
  public skip = 0;
  public limit: number = this.pageSize;
  public pageIndex = 0;
  public serialNumberArray: Array<number> = [];
  public currentPage = 1;
  public pageNumberArray: Array<number> = [];
  public pageSelection: Array<any> = [];
  public totalPages = 0;
  public books_general:any = "";
  public book_selected:any;

  constructor(
    public LibrosService: LibrosService,
  ){

  }
  ngOnInit() {
    this.getTableData();
  }
  private getTableData(): void {
    this.booksList = [];
    this.serialNumberArray = [];

    this.LibrosService.listBooks().subscribe((resp:any) => {
      console.log(resp);

      this.totalData = resp.books.length;
      this.books_general = resp.books;
      this.getTableDataGeneral();
    });
  }

  getTableDataGeneral()
  {
    this.booksList = [];
    this.serialNumberArray = [];
    this.books_general.map((res: any, index: number) => {
      const serialNumber = index + 1;
      if (index >= this.skip && serialNumber <= this.limit) {
       
        this.booksList.push(res);
        this.serialNumberArray.push(serialNumber);
      }
    });
    this.dataSource = new MatTableDataSource<any>(this.booksList);
    this.calculateTotalPages(this.totalData, this.pageSize);
  }

  selectBook(book:any)
  {
    this.book_selected = book;
  }

  deleteBook()
  {
    this.LibrosService.deleteBooks(this.book_selected.id).subscribe((resp:any) => {
      console.log(resp);
      let INDEX = this.booksList.findIndex((item:any) => item.id == this.book_selected.id);

      if(INDEX != 1)
      {
        this.booksList.splice(INDEX,1);
        this.book_selected = null;
      }
    });
  }
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public searchData(value: any): void {
    this.dataSource.filter = value.trim().toLowerCase();
    this.booksList = this.dataSource.filteredData;
  }

  public sortData(sort: any) {
    const data = this.booksList.slice();

    if (!sort.active || sort.direction === '') {
      this.booksList = data;
    } else {
      this.booksList = data.sort((a:any, b:any) => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const aValue = (a as any)[sort.active];
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const bValue = (b as any)[sort.active];
        return (aValue < bValue ? -1 : 1) * (sort.direction === 'asc' ? 1 : -1);
      });
    }
  }

  public getMoreData(event: string): void {
    if (event == 'next') {
      this.currentPage++;
      this.pageIndex = this.currentPage - 1;
      this.limit += this.pageSize;
      this.skip = this.pageSize * this.pageIndex;
      this.getTableDataGeneral();
    } else if (event == 'previous') {
      this.currentPage--;
      this.pageIndex = this.currentPage - 1;
      this.limit -= this.pageSize;
      this.skip = this.pageSize * this.pageIndex;
      this.getTableDataGeneral();
    }
  }

  public moveToPage(pageNumber: number): void {
    this.currentPage = pageNumber;
    this.skip = this.pageSelection[pageNumber - 1].skip;
    this.limit = this.pageSelection[pageNumber - 1].limit;
    if (pageNumber > this.currentPage) {
      this.pageIndex = pageNumber - 1;
    } else if (pageNumber < this.currentPage) {
      this.pageIndex = pageNumber + 1;
    }
    this.getTableDataGeneral();
  }

  public PageSize(): void {
    this.pageSelection = [];
    this.limit = this.pageSize;
    this.skip = 0;
    this.currentPage = 1;
    this.getTableDataGeneral();
  }

  private calculateTotalPages(totalData: number, pageSize: number): void {
    this.pageNumberArray = [];
    this.totalPages = totalData / pageSize;
    if (this.totalPages % 1 != 0) {
      this.totalPages = Math.trunc(this.totalPages + 1);
    }
    /* eslint no-var: off */
    for (var i = 1; i <= this.totalPages; i++) {
      const limit = pageSize * i;
      const skip = limit - pageSize;
      this.pageNumberArray.push(i);
      this.pageSelection.push({ skip: skip, limit: limit });
    }
  }
}
