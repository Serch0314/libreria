import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListLibroComponent } from './list-libro.component';

const routes: Routes = [
  {
    path: '',
    component: ListLibroComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListLibroRoutingModule { }
