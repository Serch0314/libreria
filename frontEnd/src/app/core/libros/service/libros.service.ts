import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from 'src/app/config/config';
import { AuthService } from 'src/app/shared/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class 
LibrosService {

  constructor(
    public http: HttpClient, 
    public authService: AuthService,
  ) { 

  }

  listBooks()
  {
    let hearders = new HttpHeaders({'Authorization': 'Bearer '+this.authService.token});
    let URL = URL_SERVICIOS+"/books";
    return this.http.get(URL, {headers: hearders});
  }

  showBook(book_id:string)
  {
    let hearders = new HttpHeaders({'Authorization': 'Bearer '+this.authService.token});
    let URL = URL_SERVICIOS+"/books/"+book_id;
    return this.http.get(URL, {headers: hearders});
  }

  storeBooks(data:any)
  {
    let hearders = new HttpHeaders({'Authorization': 'Bearer '+this.authService.token});
    let URL = URL_SERVICIOS+"/books";
    return this.http.post(URL, data, {headers: hearders});
  }

  editBooks(data:any, id_book:any)
  {
    let hearders = new HttpHeaders({'Authorization': 'Bearer '+this.authService.token});
    let URL = URL_SERVICIOS+"/books/"+id_book;
    return this.http.put(URL,data, {headers: hearders});
  }

  deleteBooks(id_book:any)
  {
    let hearders = new HttpHeaders({'Authorization': 'Bearer '+this.authService.token});
    let URL = URL_SERVICIOS+"/books/"+id_book;
    return this.http.delete(URL, {headers: hearders});
  }
}
