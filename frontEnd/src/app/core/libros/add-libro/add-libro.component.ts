import { Component } from '@angular/core';
import { LibrosService } from '../service/libros.service';

@Component({
  selector: 'app-add-libro',
  templateUrl: './add-libro.component.html',
  styleUrls: ['./add-libro.component.scss']
})
export class AddLibroComponent {
  titulo:any = '';
  autor:any = '';
  fecha:any = '';
  categoria:any = '';

  valid_form:boolean = false;

  valid_form_success:boolean = false;

  constructor(
    public LibrosService: LibrosService,
  ){

  }

  save()
  {
    if (!this.titulo || !this.autor || !this.fecha || !this.categoria)
    {
      this.valid_form = true;
    }
    else
    {
      let data = {
        titulo: this.titulo,
        autor: this.autor,
        fecha: this.fecha,
        categoria: this.categoria
      };

      this.valid_form = false;
      this.valid_form_success = false;

      this.LibrosService.storeBooks(data).subscribe((resp:any) => {
        console.log(resp);
        this.titulo = '';
        this.autor = '';
        this.fecha = '';
        this.categoria = '';
        this.valid_form_success = true;
      });
    }

    
  }

}
