import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddLibroRoutingModule } from './add-libro-routing.module';
import { AddLibroComponent } from './add-libro.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AddLibroComponent,
  ],
  imports: [
    CommonModule,
    AddLibroRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
  ]
})
export class AddLibroModule { }
