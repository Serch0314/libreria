<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // Filtro por titulo del libro
        $titulo = $request->titulo;

        $books = Book::where('titulo','like', '%'.$titulo.'%')->orderBy('id', 'desc')->get();

        return response()->json([
            'books' => $books->map(function($book){
                return [
                    'id' => $book->id,
                    'titulo' => $book->titulo,
                    'autor' => $book->autor,
                    'fecha' => $book->fecha,
                    'categoria' => $book->categoria
                ];
            }),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'titulo' =>'required',
            'autor' => 'required',
            'fecha' => 'required',
            'categoria' =>'required'
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "message" => 400,
                "message_text" => "Error ingrese todos los datos"
            ]);
        }

        $book = Book::create([
            'titulo' => $request->titulo,
            'autor' => $request->autor,
            'fecha' => $request->fecha,
            'categoria' => $request->categoria
        ]);

        return response()->json([
            "message" => 200
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $book = Book::findOrFail($id);

        return response()->json([
            'id' => $book->id,
            'titulo' => $book->titulo,
            'autor' => $book->autor,
            'fecha' => $book->fecha,
            'categoria' => $book->categoria
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'titulo' =>'required',
            'autor' => 'required',
            'fecha' => 'required',
            'categoria' =>'required'
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "message" => 400,
                "message_text" => "Error ingrese todos los datos"
            ]);
        }

        $book_exist = Book::where("id", '<>', $id)->where('titulo', $request->titulo)->first();
        
        if($book_exist)
        {
            return response()->json([
                "message" => 403,
                "message_text" => "El Titulo del libro ya existe"
            ]);
        }

        $book = Book::findOrFail($id);

        $book->update($request->all());

        return response()->json([
            "message" => 200
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $book = Book::findOrFail($id);

        $book->delete();

        return response()->json([
            "message" => 200
        ]);
    }
}
